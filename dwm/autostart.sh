#!/bin/sh

# compositor
#killall picom
#while pgrep -u $UID -x picom >/dev/null; do sleep 1; done
#picom &
xcompmgr &

sxhkd &
#xmodmap ~/.config/xmodmap/Xmodmap &
unclutter &

xrandr --output HDMI-0 --primary --mode 1920x1080 --rate 74.98
xrandr --output VGA-0 --mode 1440x900 --right-of HDMI-0 --rate 74.97

#set_wallpaper ~/Pictures/wallpapers/164.webp
feh --bg-fill Pictures/wallpapers/164.webp
[[ -b /dev/sde1 ]] && doas mount /dev/sde1 /home/vijay/Drive/sony/

#sxhkd
sleep 5
exec slstatus

