#/bin/bash

scriptdir="/home/vijay/.config/suckless"

cd $scriptdir/dwm/ && sudo rm -v config.h && sudo make install
cd $scriptdir/dmenu/ && sudo rm -v config.h && sudo make install
cd $scriptdir/st/ && sudo make install
cd $scriptdir/slstatus/ && sudo rm -v config.h && sudo make install
cd $scriptdir/surf/ && sudo rm -v config.h && sudo make install

