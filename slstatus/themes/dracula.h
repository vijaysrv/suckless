static const struct arg args[] = {
	/* function format          argument */

	{ run_command,     "^c#8be9fd^  %s ",        "/home/vijay/scripts/mpdicon.sh" },
	{ run_command,     "^c#f8f8f2^  %s |",       "/home/vijay/scripts/nowplaying" }, 

//{ run_command,     "^c#50fa7b^ %s ",        "mailicon.sh" },
//{ run_command,     "^c#f8f8f2^ %s |",        "newmail.sh" },

	{ run_command,     "^c#ff79c6^ %2s ",        "/home/vijay/scripts/upicon.sh" },
	{ uptime,          "^c#f8f8f2^ %s |",        NULL           }, 
	
	{ run_command,     "^c#ffb86c^ %2s ",        "/home/vijay/scripts/cpuicon.sh" },
	{ cpu_perc,        "^c#f8f8f2^ %s%% |",      NULL           },
	
	{ run_command,     "^c#f1fa8c^ %2s ",        "/home/vijay/scripts/ramicon.sh" },
	{ ram_perc,        "^c#f8f8f2^ %s%% |",      NULL           },
	
	{ run_command,     "^c#ff5555^ %2s ",        "/home/vijay/scripts/diskicon.sh" },
	{ disk_perc,       "^c#f8f8f2^  %s%% |",     "/"            },
	
//{ run_command,     "^c#ff79c6^ %2s ",          "tempicon.sh" },
//{ run_command,     "^c#f8f8f2^ %2s |",         "tail -1 ~/.config/.temp" }, 
//
//{ run_command,     "^c#59fa7b^ %2s ",          "weathericon.sh" },
//{ run_command,     "^c#f8f8f2^ %2s |",         "tail -1 ~/.config/weather" }, 
	
//{ run_command,     "^c#bd93f9^ %s ",           "pacmanicon.sh" },
//{ run_command,     "^c#f8f8f2^ %2s |",         "pacupdate.sh" },
	

	{ run_command,     "^c#8be9fd^ %s ",         "/home/vijay/scripts/volumeicon.sh" },
	{ run_command,     "^c#f8f8f2^ %s |",        "/home/vijay/scripts/volume.sh" },

	{ run_command,     "^c#f1fa8c^ %2s ",        "/home/vijay/scripts/timeicon.sh" },
	{ datetime,        "^c#f8f8f2^ %s",          "%a %b %d - %I:%M %p" },
};
